/**
 * Mohsin Rehman
 * A12077755
 * PA2
 *
 * File: uncompress.cpp
 * This file contains the program to uncompress any file that
 * was compressed using compress.cpp. Any file that was compressed
 * with any program other than compress.cpp (even if it uses Huffman's
 * compression algorithm) will likely not work with this program.
 */

#include <iostream>
#include <fstream>
#include <vector>
#include "HCTree.hpp"

#define PROGRAM_ARGUMENTS 2
#define INPUT_FILE 1
#define OUTPUT_FILE 2

using namespace std;

/**
 * This function will uncompress the specified file and will
 * produce the original file before it was compressed using
 * compress.cpp.
 */
int main(int argc, char* argv[])
{

    // Check if arguments are valid.
    if (argc < PROGRAM_ARGUMENTS+1)
    {
        cout << argv[0] << " called with incorrect arguments." << endl;
        cout << "Usage: " << argv[0] << " infile outfile" << endl;
        return -1;
    }

    // Open input file.
    ifstream ifs(argv[INPUT_FILE], ios::in | ios::binary);
    if (!ifs.is_open())
    {
        cout << "Error opening \"" << argv[INPUT_FILE] << "\" for input." << endl;
        return -1;
    }

    // Open output file.
    ofstream ofs(argv[OUTPUT_FILE]);
    if (!ofs.is_open())
    {
        cout << "Error opening \"" << argv[OUTPUT_FILE] << "\" for output." << endl;
        ifs.close();
        return -1;
    }

    // Create a bit input stream object for the input file.
    BitInputStream bis(&ifs);

    // Build Huffman tree from the input file.
    HCTree tree;
    int count = tree.build(bis);

    // Decode each character from the input file into the
    // output file.
    for (int i = 0; i < count; i++)
    {
        ofs.put(tree.decode(bis));
    }

    // Close input and output file streams.
    ifs.close();
    ofs.close();
    return 0;

}
