/**
 * Mohsin Rehman
 * A12077755
 * PA2
 *
 * File: BitInputStream.cpp
 * This file contains the code for the BitInputStream
 * class which can read data from a file one bit at a time.
 */

#include "BitInputStream.hpp"

/**
 * Gets one bit from the stream.
 * Returns 1 or 0.
 */
int BitInputStream::get()
{

    // Increase index by one and buffer the next byte of the input stream
    // if necessary.
    if (++idx >= BITS_PER_BYTE)
    {
        buf = ifs->get();
        idx = 0;
    }

    // Return the next bit in the buffer.
    return ((buf >> idx) & 1);
}

/**
 * Gets the next 8 bits from the stream
 * and stores them into a byte.
 */
byte BitInputStream::getByte()
{

    // load 8 bits and store them into a byte.
    byte b = 0;
    for (int i = 0; i < BITS_PER_BYTE; i++)
    {
        if (get())
            b = (b | (1 << i));
    }

    // return the byte containing the 8 bits.
    return b;
}
