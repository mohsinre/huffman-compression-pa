/**
 * Mohsin Rehman
 * A12077755
 * PA2
 *
 * File: HCNode.cpp
 * This file contains the code for the HCNode class which
 * is to be used with the HCTree class to create a representation
 * of a Huffman tree.
 */

#include "HCNode.hpp"

/**
 * Less-than comparison operator for HCNode objects
 * so that they can work with std::priority_queue.
 * Smaller frequencies have higher priority.
 * The symbol used by this node is used to make this
 * function non-deterministic.
 */
bool HCNode::operator<(const HCNode& other)
{
    // Use the nodes' symbols to make this function non-deterministic.
    if (count == other.count)
        return symbol > other.symbol;

    // Compare the frequency counts of the two nodes.
    return count > other.count;
}
