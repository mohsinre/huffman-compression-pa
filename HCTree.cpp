/**
 * Mohsin Rehman
 * A12077755
 * PA2
 *
 * File: HCTree.cpp
 * This file contains the code for the HCTree class
 * which represents a Huffman tree that can be used to
 * encode and decode characters into bits.
 */

#include "HCTree.hpp"

#define BITS_PER_BYTE 8
#define FREQUENCY_BYTES 3
#define BYTE_MASK 0xFF
#define FIRST_BYTE 0
#define SECOND_BYTE 8
#define THIRD_BYTE 16

/**
 * HCTree destructor function:
 * Delete all the nodes that were allocated in the heap.
 */
HCTree::~HCTree()
{
    deleteNodeRecursive(root);
}

/**
 * Recursive method to delete all the nodes in the
 * Huffman tree. First all the children of the current
 * node are deleted, then the node itself is deleted.
 */
void HCTree::deleteNodeRecursive(HCNode *node)
{
    if (!node)
        return;

    if (node->c0)
        deleteNodeRecursive(node->c0);

    if (node->c1)
        deleteNodeRecursive(node->c1);

    delete node;
}


/** Use the Huffman algorithm to build a Huffman coding trie.
 *  PRECONDITION: freqs is a vector of ints, such that freqs[i] is 
 *  the frequency of occurrence of byte i in the message.
 *  POSTCONDITION:  root points to the root of the trie,
 *  and leaves[i] points to the leaf node containing byte i.
 */
void HCTree::build(const vector<int>& freqs)
{

    // Create priority queue and insert all non-zero
    // frequency nodes into the priority queue and the leaves vector.
    priority_queue<HCNode*,vector<HCNode*>,HCNodePtrComp> pq;
    for (int i = 0; i < TOTAL_BYTE_VALUES; i++)
    {
        if (freqs[i] > 0)
        {
            totalFrequency += freqs[i];
            HCNode *node = new HCNode(freqs[i], (byte)i);
            leaves[i] = node;
            pq.push(node);
        }
    }

    // Check if there are no characters at all in the file.
    if (pq.size() == 0)
        return;

    // Keep merging two lowest frequency trees until there is only
    // one tree left.
    while (pq.size() > 1)
    {

        // Get the two lowest frequency trees.
        HCNode *n1 = pq.top();
        pq.pop();
        HCNode *n2 = pq.top();
        pq.pop();

        // Merge the two trees and put the merged tree in the queue.
        HCNode *n3 = new HCNode(n1->count + n2->count,
                                n1->symbol, n1, n2);
        n1->p = n3;
        n2->p = n3;
        pq.push(n3);

    }

    // Set the root of the tree.
    root = pq.top();

}

/**
 * Build this tree based on the bits from the specified bit
 * input stream.
 */
int HCTree::build(BitInputStream& in)
{
    
    // Read the total frequency.
    for (int i = 0; i < FREQUENCY_BYTES; i++)
        totalFrequency = (totalFrequency << BITS_PER_BYTE) + in.getByte();

    // Build the tree and store in root variable.
    buildSubTree(&root, in);

    // Return the total frequency.
    return totalFrequency;

}

/**
 * Helper function for building this tree based on a bit
 * representation that was read from an input file.
 */
void HCTree::buildSubTree(HCNode **loc, BitInputStream& in)
{

    // If the next node is a leaf
    if (in.get())
    {
        byte symbol = in.getByte();
        *loc = new HCNode(0, symbol);
        leaves[symbol] = (*loc);
    }
    else
    {
        // Create the parent node.
        *loc = new HCNode(0, 0);

        // Build left sub tree.
        buildSubTree(&((*loc)->c0), in);
        (*loc)->c0->p = (*loc);

        // Build right sub tree.
        buildSubTree(&((*loc)->c1), in);
        (*loc)->c1->p = (*loc);
    }

}

/**
 * Write a bit representation of this tree into the
 * specified bit output stream.
 */
void HCTree::writeHeader(BitOutputStream& out) const
{

    // Write the total frequency to file.
    byte mask = BYTE_MASK;
    out.putByte(mask & (totalFrequency >> THIRD_BYTE));
    out.putByte(mask & (totalFrequency >> SECOND_BYTE));
    out.putByte(mask & (totalFrequency >> FIRST_BYTE));

    // Write the tree using pre order traversal.
    if (root)
        writeNodePreOrder(root, out);
    
}

/**
 * Writes a representation of this tree in bits to the specified
 * bit output stream.
 */
void HCTree::writeNodePreOrder(HCNode *node, BitOutputStream& out) const
{
    // If this node is not a leaf, just output 0 for this node
    // and then write data for left and right sub-trees.
    if (node->c0)
    {
        out.put(0);
        writeNodePreOrder(node->c0, out);
        writeNodePreOrder(node->c1, out);
    }

    // Else since this is a leaf, output 1 and the symbol for this leaf.
    else
    {
        out.put(1);
        out.putByte(node->symbol);
    }
}

/** Write to the given BitOutputStream
 *  the sequence of bits coding the given symbol.
 *  PRECONDITION: build() has been called, to create the coding
 *  tree, and initialize root pointer and leaves vector.
 */
void HCTree::encode(byte symbol, BitOutputStream& out) const
{
    
    // Get the leaf node for this symbol.
    HCNode *leaf = leaves[symbol];
    string code = "";
    
    // Get the code for this symbol.
    while (leaf->p)
    {
        if (leaf->p->c0 == leaf)
            code = "0" + code;
        else
            code = "1" + code;

        leaf = leaf->p;
    }

    // Output the code to the output stream.
    for (int i = 0; i < code.size(); i++)
    {
        if (code[i] == '1')
            out.put(1);
        else
            out.put(0);
    }

}

/** Write to the given ofstream
 *  the sequence of bits (as ASCII) coding the given symbol.
 *  PRECONDITION: build() has been called, to create the coding
 *  tree, and initialize root pointer and leaves vector.
 *  THIS METHOD IS USEFUL FOR THE CHECKPOINT BUT SHOULD NOT 
 *  BE USED IN THE FINAL SUBMISSION.
 */
void HCTree::encode(byte symbol, ofstream& out) const
{
    
    // Get the leaf node for this symbol.
    HCNode *leaf = leaves[symbol];
    string code = "";
    
    // Get the code for this symbol.
    while (leaf->p)
    {
        if (leaf->p->c0 == leaf)
            code = "0" + code;
        else
            code = "1" + code;

        leaf = leaf->p;
    }

    // Output the code to the output stream.
    out << code;

}

/** Return symbol coded in the next sequence of bits from the stream.
 *  PRECONDITION: build() has been called, to create the coding
 *  tree, and initialize root pointer and leaves vector.
 */
int HCTree::decode(BitInputStream& in) const
{

    // Start with the root node.
    HCNode *working = root;

    // Traverse the tree until you reach a leaf.
    while (working->c0)
    {
        if (!in.get())
            working = working->c0;
        else
            working = working->c1;
    }

    // Return the symbol represented by this leaf node.
    return working->symbol;

}

/** Return the symbol coded in the next sequence of bits (represented as 
 *  ASCII text) from the ifstream.
 *  PRECONDITION: build() has been called, to create the coding
 *  tree, and initialize root pointer and leaves vector.
 *  THIS METHOD IS USEFUL FOR THE CHECKPOINT BUT SHOULD NOT BE USED
 *  IN THE FINAL SUBMISSION.
 */
int HCTree::decode(ifstream& in) const
{

    // Start with the root node.
    HCNode *working = root;

    // Traverse the tree until you reach a leaf.
    while (working->c0)
    {
        byte b = in.get();
        if (b == '0')
            working = working->c0;
        else
            working = working->c1;
    }

    // Return the symbol represented by this leaf node.
    return working->symbol;

}
