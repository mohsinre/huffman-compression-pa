/**
 * Mohsin Rehman
 * A12077755
 * PA2
 *
 * File: BitInputStream.hpp
 * This file contains the header code for the BitInputStream
 * class which can read data from a file one bit at a time.
 */

#ifndef BITINPUTSTREAM_HPP
#define BITINPUTSTREAM_HPP

#include <fstream>

#define BITS_PER_BYTE 8

typedef unsigned char byte;

using namespace std;

/**
 * This class can read data from a file
 * one bit at a time.
 */
class BitInputStream {

private:

  /* The input file stream this bit stream is reading from. */
  ifstream *ifs;

  /* The index of the next bit in the byte. */
  int idx;

  /* The byte that is buffered to read bits from. */
  byte buf;

public:
  BitInputStream(ifstream *stream) : 
    ifs(stream), idx(BITS_PER_BYTE), buf(0) { }

  /**
   * Gets one bit from the stream.
   * Returns 1 or 0.
   */
  int get();

  /**
   * Gets the next 8 bits from the stream
   * and stores them into a byte.
   */
  byte getByte();

};

#endif // BITINPUTSTREAM_HPP
