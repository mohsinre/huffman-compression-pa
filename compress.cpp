/**
 * Mohsin Rehman
 * A12077755
 * PA2
 *
 * File: compress.cpp
 * This file contains the program used to compress any file
 * using the Huffman compression algorithm. Any file compressed
 * with this program can be uncompressed with uncompress.cpp
 */

#include <iostream>
#include <fstream>
#include <vector>
#include "HCTree.hpp"

#define PROGRAM_ARGUMENTS 2
#define TOTAL_BYTE_VALUES 256
#define INPUT_FILE 1
#define OUTPUT_FILE 2

using namespace std;

/**
 * This function will produce a compressed version of the specified
 * file using the Huffman compression algorithm.
 */
int main(int argc, char* argv[])
{

    // Check if arguments are valid.
    if (argc < PROGRAM_ARGUMENTS+1)
    {
        cout << argv[0] << " called with incorrect arguments." << endl;
        cout << "Usage: " << argv[0] << " infile outfile" << endl;
        return -1;
    }

    // Open input file.
    ifstream ifs(argv[INPUT_FILE]);
    if (!ifs.is_open())
    {
        cout << "Error opening \"" << argv[INPUT_FILE] << "\" for input." << endl;
        return -1;
    }

    // Open output file.
    ofstream ofs(argv[OUTPUT_FILE], ios::out | ios::binary);
    if (!ofs.is_open())
    {
        cout << "Error opening \"" << argv[OUTPUT_FILE] << "\" for output." << endl;
        ifs.close();
        return -1;
    }

    // Create bit output stream for output file.
    BitOutputStream bos(&ofs);


    // Get character frequencies.
    vector<int> freqs = vector<int>(TOTAL_BYTE_VALUES, 0);
    while (true)
    {
        byte b = ifs.get();
        if (ifs.eof())
            break;
        freqs[b]++;
    }

    // Build Huffman tree and write it to output file.
    HCTree tree;
    tree.build(freqs);
    tree.writeHeader(bos);

    // Encode each character from the input file into the
    // output file.
    ifs.clear();
    ifs.seekg(0, ios::beg);
    while (true)
    {
        byte b = ifs.get();
        if (ifs.eof())
            break;
        tree.encode(b, bos);
    }

    // Close input and output file streams.
    bos.flush();
    ifs.close();
    ofs.close();
    return 0;

}
