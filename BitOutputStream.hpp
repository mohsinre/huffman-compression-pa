/**
 * Mohsin Rehman
 * A12077755
 * PA2
 *
 * File: BitOutputStream.hpp
 * This file contains the header code for the BitOutputStream
 * class which can write to a file one bit at a time.
 */

#ifndef BITOUTPUTSTREAM_HPP
#define BITOUTPUTSTREAM_HPP

#include <fstream>

#define BITS_PER_BYTE 8

typedef unsigned char byte;

using namespace std;

/**
 * This class can write data to a file one bit at a time.
 */
class BitOutputStream {

private:

  /* The output stream to write the data to. */
  ofstream *ofs;

  /* The index in the buffer to store the next bit in. */
  int idx;

  /* The buffer to hold 8 bits. */
  byte buf;

public:
  BitOutputStream(ofstream *stream) : ofs(stream), idx(0), buf(0) { }

  /**
   * Puts one bit into the output file. It is actually stored
   * into a buffer which is automatically written to the file
   * once it contains 8 bits.
   */
  void put(int bit);

  /**
   * Writes a byte to the output file.
   */
  void putByte(byte b);

  /**
   * Writes out the buffer to the output file if there is any
   * buffered data.
   */
  void flush();

};

#endif // BITOUTPUTSTREAM_HPP
