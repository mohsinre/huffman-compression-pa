/**
 * Mohsin Rehman
 * A12077755
 * PA2
 *
 * File: BitOutputStream.cpp
 * This file contains the code for the BitInputStream
 * class which can write to a file one bit at a time.
 */

#include "BitOutputStream.hpp"

/**
 * Puts one bit into the output file. It is actually stored
 * into a buffer which is automatically written to the file once
 * it contains 8 bits.
 */
void BitOutputStream::put(int bit)
{
    // Add the bit to the buffer.
    if (bit)
        buf = (buf | (1 << idx));

    // If the buffer is full then write it to output file.
    if (++idx >= BITS_PER_BYTE)
        flush();
}

/**
 * Writes a byte to the output file.
 */
void BitOutputStream::putByte(byte b)
{
    // Write all the bits into the output file.
    for (int i = 0; i < BITS_PER_BYTE; i++)
    {
        put((b >> i) & 1);
    }
}

/**
 * Writes out the buffer to the output file if there is any
 * buffered data.
 */
void BitOutputStream::flush()
{
    // If there is any buffered data
    if (idx)
    {

        // Write buffer to the output file.
        ofs->put(buf);
        buf = idx = 0;
    }
}
