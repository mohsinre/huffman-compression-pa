/**
 * Mohsin Rehman
 * A12077755
 * PA2
 *
 * File: HCTree.hpp
 * This file contains the header code for the HCTree class
 * which represents a Huffman tree that can be used to
 * encode and decode characters into bits.
 */

#ifndef HCTREE_HPP
#define HCTREE_HPP

#include <queue>
#include <vector>
#include <fstream>
#include "HCNode.hpp"
#include "BitInputStream.hpp"
#include "BitOutputStream.hpp"

#define TOTAL_BYTE_VALUES 256

using namespace std;

/** A 'function class' for use as the Compare class in a
 *  priority_queue<HCNode*>.
 *  For this to work, operator< must be defined to
 *  do the right thing on HCNodes.
 */
class HCNodePtrComp {
public:
    bool operator()(HCNode*& lhs, HCNode*& rhs) const {
        return *lhs < *rhs;
    }
};

/** A Huffman Code Tree class.
 *  Not very generic:  Use only if alphabet consists
 *  of unsigned chars.
 */
class HCTree {
private:

    /* Pointer to the root node of the tree. */
    HCNode* root;

    /* Vector containing pointers to all the leaves in the tree. */
    vector<HCNode*> leaves;

    /* The total number of bytes in the original file. */
    int totalFrequency;

    /**
     * Deletes the specified node and all nodes in any sub-tree of that
     * node recursively.
     */
    void deleteNodeRecursive(HCNode *node);

    /**
     * Writes a representation of this tree in bits to the specified
     * bit output stream.
     */
    void writeNodePreOrder(HCNode *node, BitOutputStream& out) const;

    /**
     * Helper function for building this tree based on a bit
     * representation that was read from an input file.
     */
    void buildSubTree(HCNode **loc, BitInputStream& in);

public:
    explicit HCTree() : root(0), totalFrequency(0) {
        leaves = vector<HCNode*>(TOTAL_BYTE_VALUES, (HCNode*) 0);
    }

    /**
     * Delete all memory taken up by nodes in this tree.
     */
    ~HCTree();

    /** Use the Huffman algorithm to build a Huffman coding trie.
     *  PRECONDITION: freqs is a vector of ints, such that freqs[i] is 
     *  the frequency of occurrence of byte i in the message.
     *  POSTCONDITION:  root points to the root of the trie,
     *  and leaves[i] points to the leaf node containing byte i.
     */
    void build(const vector<int>& freqs);

    /**
     * Build this tree based on the bits from the specified bit
     * input stream.
     */
    int build(BitInputStream& in);

    /**
     * Write a bit representation of this tree into the
     * specified bit output stream.
     */
    void writeHeader(BitOutputStream& out) const;

    /** Write to the given BitOutputStream
     *  the sequence of bits coding the given symbol.
     *  PRECONDITION: build() has been called, to create the coding
     *  tree, and initialize root pointer and leaves vector.
     */
    void encode(byte symbol, BitOutputStream& out) const;

    /** Write to the given ofstream
     *  the sequence of bits (as ASCII) coding the given symbol.
     *  PRECONDITION: build() has been called, to create the coding
     *  tree, and initialize root pointer and leaves vector.
     *  THIS METHOD IS USEFUL FOR THE CHECKPOINT BUT SHOULD NOT 
     *  BE USED IN THE FINAL SUBMISSION.
     */
    void encode(byte symbol, ofstream& out) const;


    /** Return symbol coded in the next sequence of bits from the stream.
     *  PRECONDITION: build() has been called, to create the coding
     *  tree, and initialize root pointer and leaves vector.
     */
    int decode(BitInputStream& in) const;

    /** Return the symbol coded in the next sequence of bits (represented as 
     *  ASCII text) from the ifstream.
     *  PRECONDITION: build() has been called, to create the coding
     *  tree, and initialize root pointer and leaves vector.
     *  THIS METHOD IS USEFUL FOR THE CHECKPOINT BUT SHOULD NOT BE USED
     *  IN THE FINAL SUBMISSION.
     */
    int decode(ifstream& in) const;

};

#endif // HCTREE_HPP
